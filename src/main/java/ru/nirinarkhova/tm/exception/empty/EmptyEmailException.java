package ru.nirinarkhova.tm.exception.empty;

import ru.nirinarkhova.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error! Email is empty...");
    }

}
