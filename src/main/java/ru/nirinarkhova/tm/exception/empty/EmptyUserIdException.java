package ru.nirinarkhova.tm.exception.empty;

import ru.nirinarkhova.tm.exception.AbstractException;

public class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error! User Id is empty...");
    }

}
