package ru.nirinarkhova.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectFinishByIdCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-finish-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "change project status to Complete by project id.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Optional<Project> project = serviceLocator.getProjectService().finishById(userId, id);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

}

