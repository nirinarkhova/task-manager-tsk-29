package ru.nirinarkhova.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.nirinarkhova.tm.api.entity.ILoggerService;
import ru.nirinarkhova.tm.api.repository.*;
import ru.nirinarkhova.tm.api.service.*;
import ru.nirinarkhova.tm.command.AbstractCommand;
import ru.nirinarkhova.tm.component.Backup;
import ru.nirinarkhova.tm.enumerated.Role;
import ru.nirinarkhova.tm.exception.system.UnknownArgumentException;
import ru.nirinarkhova.tm.exception.system.UnknownCommandException;
import ru.nirinarkhova.tm.repository.CommandRepository;
import ru.nirinarkhova.tm.repository.ProjectRepository;
import ru.nirinarkhova.tm.repository.TaskRepository;
import ru.nirinarkhova.tm.repository.UserRepository;
import ru.nirinarkhova.tm.service.*;
import ru.nirinarkhova.tm.util.SystemUtil;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Set;

@Getter
public class Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private  final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private  final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private  final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    public final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull private final Backup backup = new Backup(this, propertyService);

    private void initData() {
        String testId = userService.create("user1", "user", "user@test.ru").getId();
        projectService.add(testId, "Project1", "123");
        taskService.add(testId, "Task1", "desc");
        taskService.add(testId, "Task2", "desc");
        taskService.add(testId, "Task3", "desc");
        String adminId = userService.create("admin1", "admin", Role.ADMIN).getId();
        projectService.add(adminId, "Project2", "desc");
        taskService.add(adminId, "Task4", "desc");
        taskService.add(adminId, "Task5", "desc");
        taskService.add(adminId, "Task6", "desc");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }


    public void run(final String... args) throws UnknownArgumentException {
        loggerService.info("***WELCOME TO TASK MANAGER***");
        if (parseArgs(args)) System.exit(0);
        initCommands();
        initData();
        initPID();
        backup.load();
        backup.init();
        while (true){
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.err.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.nirinarkhova.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.nirinarkhova.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    private void registry(final AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArg(final String arg) {
        if (!Optional.ofNullable(arg).isPresent()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownCommandException(arg);
        command.execute();
    }

    public boolean parseArgs(String[] args) throws UnknownArgumentException {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(final String cmd) {
        if (!Optional.ofNullable(cmd).isPresent()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownCommandException(cmd);
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

}