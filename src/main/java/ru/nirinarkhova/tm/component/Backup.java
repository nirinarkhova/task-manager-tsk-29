package ru.nirinarkhova.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.nirinarkhova.tm.api.service.IPropertyService;
import ru.nirinarkhova.tm.bootstrap.Bootstrap;

public class Backup extends Thread {

    @NotNull
    private static final String BACKUP_SAVE = "backup-save";

    @NotNull
    private static final String BACKUP_LOAD = "backup-load";

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    public final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap, @NotNull final IPropertyService propertyService) {
        this.bootstrap = bootstrap;
        this.propertyService = propertyService;
        setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(propertyService.getBackupInterval());
        }
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        bootstrap.parseCommand(BACKUP_SAVE);
    }

    public void load() {
        bootstrap.parseCommand(BACKUP_LOAD);
    }

}

